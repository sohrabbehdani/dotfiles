# My DWM



## Getting started

```
sudo make clean
make
sudo make install

```

The terminal that i used in this config is Kitty and the file manager is pcmanfm

```
sudo pacman -S kitty pcmanfm dmenu

```

## create a desktop file

in order to run dwm from a login manager you need to make a desktop file for it.
here is an example:

```
[Desktop Entry]
Name= Dwm
Comment= Dynamic window manager
Exec=dwm
Type=Application

```

## AutoStarting applications

For autostarting you need to make a file called autostart.sh in ~/.dwm and then add the apps you want to start to it.

here is an example:

```
#!/bin/bash
nm-applet &
picom &
nitrogen --restore &
volumeicon

```

## keybindings

launching a terminal:

META + enter

launching dmenu:

META + P

launching file manager (pcmanfm):

META + Shift + Enter
